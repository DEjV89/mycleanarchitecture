package pl.dejv.cleanarchitecture.api.model;

import java.util.Date;

/**
 * Created by dejv on 23/10/2016.
 */

public class Message {

	private Long id;
	private final String content;
	private final Date created;
	private STATE state;

	public Message(String content) {
		id = null;
		this.content = content;
		created = new Date();
		state = STATE.PENDING;
	}

	public void send() {
		if (state == STATE.CANCELLED) {
			throw new IllegalStateException();
		}
		state = STATE.SENT;
	}

	public void cancel() {
		if (state == STATE.SENT)
			throw new IllegalStateException();
		state = STATE.CANCELLED;
	}

	public STATE getState() {
		return state;
	}

	public String getContent() {
		return content;
	}

	public enum STATE {
		PENDING, SENT, CANCELLED;
	}
}
