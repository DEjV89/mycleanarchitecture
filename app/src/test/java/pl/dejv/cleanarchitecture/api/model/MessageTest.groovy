package pl.dejv.cleanarchitecture.api.model

import spock.lang.Specification

/**
 * Created by dejv on 23/10/2016.
 */
class MessageTest extends Specification {
	def message

	def "should have PENDING state after creation"() {

		when:
		message = getNewMessage()

		then:
		message.getState() == Message.STATE.PENDING
	}

	def "should have content after creation"() {

		def content
		given:
		content = "Specific content"

		when:
		message = getNewMessage(content);

		then:
		message.getContent() == content
	}

	def "should have SENT state after sending"() {

		given:
		message = getNewMessage();

		when:
		message.send()

		then:
		message.getState() == Message.STATE.SENT
	}

	def "should have CANCELLED state after cancellation"() {

		given:
		message = getNewMessage()

		when:
		message.cancel()

		then:
		message.getState() == Message.STATE.CANCELLED
	}

	def "should not be able to cancel message after it is sent"() {
		given:
		message = getNewMessage()

		when:
		message.send()
		message.cancel()

		then:
		thrown(IllegalStateException)
	}

	def "should not be able to send message after it is canceled" () {
		given:
		message = getNewMessage()

		when:
		message.cancel()
		message.send()

		then:
		thrown(IllegalStateException)
	}


	private Message getNewMessage() {
		return new Message("Default content");
	}

	private Message getNewMessage(String content) {
		return new Message(content)
	}
}
